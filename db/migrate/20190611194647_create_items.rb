class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :task_name
      t.string :priority
      t.references :board, foreign_key: true

      t.timestamps
    end
  end
end
