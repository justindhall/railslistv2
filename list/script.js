function fetchPost() {
    newItem = {
        task_name: $('input')[0].value,
        board_id: 1
    }

    fetch(url, {
        method: 'POST',
        body: JSON.stringify(newItem)
    })
        .then(function (data) {
            console.log('Request success: ', data);
        })
        .catch(function (error) {
            console.log('Request failure: ', error);
        });
}

let items = []
let url ='http://localhost:3000/items'

function fetchItems() {
    fetch(url)
        .then(response => response.json())
        .then(data => items = data)
        .then(() => console.log(items))
        .then(listItemsFromDb);
}

function addTask() {
    if ($('input')[0].value != '') {
        let li = document.createElement("LI")
        let text = document.createElement("SPAN")
        text.innerText = $('input')[0].value
        let edit = document.createElement("button")
        edit.type = "button"
        edit.innerText = "Edit"
        edit.id = "edit"
        let checkbox = document.createElement('input')
        checkbox.type = "checkbox"
        li.appendChild(text)
        li.appendChild(edit)
        li.appendChild(checkbox)
        document.querySelector("ul").appendChild(li)
        $('input')[0].value = ''
    }
}

function listItemsFromDb() {
    items.forEach(function(item) {
        $('input')[0].value = item.task_name
        addTask()
    })
}

$(document).ready(function () {

    fetchItems()

    // setTimeout(function() {
    //     listItemsFromDb();
    // }, 30);

    document.querySelector("#submit").onclick = addTask;
    document.querySelector("#delete-list").onclick = deleteList;
    document.querySelector("#delete-completed").onclick = deleteCompletedTasks;
    $(document).on('click', '#ul-list li #edit', createEditBox)
    $(document).on('click', '#ul-list li #submit-edit', submitEdit)

    function deleteList() {
        let ul = document.querySelector('ul')
        ul.innerHTML = ''
    }

    function deleteCompletedTasks() {
        var boxes = $('input:checkbox')
        for (var x in boxes) {
            if(boxes[x].checked == true){
                boxes[x].parentNode.remove()
            }
        }
    }

    function createEditBox(event) {
        var button = event.target
        var text = button.parentNode.firstChild.innerText
        var thisLi = button.parentNode
        editBox = document.createElement("input")
        editBox.type = "text"
        editBox.placeholder = text
        submitEdit = document.createElement("button")
        submitEdit.type = "button"
        submitEdit.innerText = "Edit"
        submitEdit.id = "submit-edit"
        thisLi.firstChild.remove()
        thisLi.firstChild.remove()
        thisLi.prepend(submitEdit)
        thisLi.prepend(editBox)
    }

    function submitEdit(event) {
        var button = event.target
        var text = button.parentNode.firstChild.value
        var thisLi = button.parentNode
        updatedTask = document.createElement("SPAN")
        if (text != '') {
            updatedTask.innerText = text
        } else {
            updatedTask.innerText = button.parentNode.firstChild.placeholder
        }
        let edit = document.createElement("button")
        edit.type = "button"
        edit.innerText = "Edit"
        edit.id = "edit"
        thisLi.firstChild.remove()
        thisLi.firstChild.remove()
        thisLi.prepend(edit)
        thisLi.prepend(updatedTask)
    }
});
