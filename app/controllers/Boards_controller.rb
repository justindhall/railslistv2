class BoardsController < ApplicationController
  protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token
  def index
    render json: Board.where(board_params)
  end

  def show
    render json: Board.find(params[:id])
  end

  def create
    if board = Board.create(board_params)
      render json: board
    else
      render json: board.errors.full_messages
    end
  end

  def update
    if board = Board.find(board_params[:id]).update(board_params)
      render json: board
    else
      render json: board.errors.full_messages
    end
  end

  def destroy
    render json: Board.find(params[:id]).destroy
  end

  private
  def board_params
    params.permit(:name)
  end
end