class ItemsController < ApplicationController
  protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token
  def index
    render json: Item.where(item_params)
  end

  def show
    render json: Item.find(params[:id])
  end

  def create
    if item = Item.create(item_params)
      render json: item
    else
      render json: item.errors.full_messages
    end
  end

  def update
    if item = Item.find(item_params[:id]).update(item_params)
      render json: item
    else
      render json: item.errors.full_messages
    end
  end

  def destroy
    render json: Item.find(params[:id]).destroy
  end

  private
  def item_params
    params.permit(:task_name, :priority, :board_id)
  end
end