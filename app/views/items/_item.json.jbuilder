json.extract! item, :id, :task_name, :priority, :board_id, :created_at, :updated_at
json.url item_url(item, format: :json)
